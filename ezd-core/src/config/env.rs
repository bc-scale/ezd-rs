use std::path::PathBuf;

use thiserror::Error;

use super::ezd::find_config_path;
use super::ezd::FindConfigPathError;

#[derive(Debug, Error)]
pub enum EzdEnvVarsError {
    #[error("ExecutablePathNotReadableError: `{0}`")]
    ExecutablePathNotReadableError(#[from] std::io::Error),

    #[error("FindConfigPathError: `{0}`")]
    FindConfigPathError(#[from] FindConfigPathError),
}

pub struct EzdEnvVars {
    pub ezd_executable: PathBuf,
    pub user_id: u32,
    pub user_name: String,
    pub group_id: u32,
    pub project_root: PathBuf,
}

impl EzdEnvVars {
    pub fn new() -> Result<Self, EzdEnvVarsError> {
        let user_name: String = match users::get_current_username() {
            Some(name) => name.to_string_lossy().into(),
            None => "user_unknown".into(),
        };
        Ok(EzdEnvVars {
            ezd_executable: std::env::current_exe()?,
            user_id: users::get_current_uid(),
            user_name,
            group_id: users::get_current_gid(),
            project_root: find_config_path()?,
        })
    }
}
