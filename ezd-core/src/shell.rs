use std::collections::HashMap;
use std::process::Command;
use std::process::Stdio;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum ShellError {
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),
    #[error("CommandFailed: `{0}`")]
    CommandFailed(String),
}

pub fn execute(command: &str, envs: &HashMap<String, String>) -> Result<(), ShellError> {
    let result = Command::new("/usr/bin/env")
        .args(vec!["sh", "-c", command])
        .envs(envs)
        .stderr(Stdio::inherit())
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .status()?;
    match result.success() {
        true => Ok(()),
        false => Err(ShellError::CommandFailed(command.into())),
    }
}
