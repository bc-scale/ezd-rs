---
tasks:
  shell_exec:
    all:
      description: check everything and build for every platform
      commands:
        - ezd run check
        - ezd run build/all/debug
        - ezd run build/all/release
    check:
      description: check everything
      commands:
        - ezd run check/fmt
        - ezd run check/clippy
        - ezd run check/test
    fix:
      description: autofix with all linters
      commands:
        - ezd run fix/fmt
        - ezd run fix/clippy
    clean:
      description: delete build artifacts
      commands: [ezd run rust:x86_64-unknown-linux-musl -- cargo clean]
    check/fmt:
      description: autofix with linter `cargo fmt`
      commands: [ezd run rust:x86_64-unknown-linux-musl -- cargo +nightly fmt -- --check]
    fix/fmt:
      description: autofix with linter `cargo fmt`
      commands: [ezd run rust:x86_64-unknown-linux-musl -- cargo +nightly fmt]
    check/clippy:
      description: autofix with linter `cargo clippy`
      commands: [ezd run rust:x86_64-unknown-linux-musl -- cargo +nightly clippy --all-targets --all-features -- -D warnings]
    fix/clippy:
      description: autofix with linter `cargo clippy`
      commands: [ezd run rust:x86_64-unknown-linux-musl -- cargo +nightly clippy --fix --allow-dirty --allow-staged --all-targets --all-features -- -D warnings]
    check/test:
      description: autofix with linter `cargo clippy`
      commands: [ezd run rust:x86_64-unknown-linux-musl -- cargo-zigbuild test --target=x86_64-unknown-linux-musl]
    build/debug:
      description: build the application in `debug` mode
      commands: [ezd run build/x86_64-unknown-linux-musl/debug]
    build/release:
      description: build the application in `release` mode
      commands: [ezd run build/x86_64-unknown-linux-musl/release]
    build/all/debug:
      description: build the application for all platforms in `debug` mode
      commands:
        - ezd run build/x86_64-unknown-linux-musl/debug
        - ezd run build/aarch64-unknown-linux-musl/debug
        - ezd run build/arm-unknown-linux-musleabihf/debug
        - ezd run build/x86_64-apple-darwin/debug
        - ezd run build/aarch64-apple-darwin/debug
    build/all/release:
      description: build the application for all platforms in `release` mode
      commands:
        - ezd run build/x86_64-unknown-linux-musl/release
        - ezd run build/aarch64-unknown-linux-musl/release
        - ezd run build/arm-unknown-linux-musleabihf/release
        - ezd run build/x86_64-apple-darwin/release
        - ezd run build/aarch64-apple-darwin/release
    build/x86_64-unknown-linux-musl/debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:x86_64-unknown-linux-musl -- "cargo zigbuild --target=x86_64-unknown-linux-musl"]
    build/x86_64-unknown-linux-musl/release:
      description: build the application in `release` mode
      commands:
        - |
          ezd run rust:x86_64-unknown-linux-musl -- "cargo +nightly zigbuild -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort --target=x86_64-unknown-linux-musl --release"
    build/aarch64-unknown-linux-musl/debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:aarch64-unknown-linux-musl -- "cargo zigbuild --target=aarch64-unknown-linux-musl"]
    build/aarch64-unknown-linux-musl/release:
      description: build the application in `release` mode
      commands: [ezd run rust:aarch64-unknown-linux-musl -- "cargo zigbuild --target=aarch64-unknown-linux-musl --release"]
    build/arm-unknown-linux-musleabihf/debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:arm-unknown-linux-musleabihf -- "cargo zigbuild --target=arm-unknown-linux-musleabihf"]
    build/arm-unknown-linux-musleabihf/release:
      description: build the application in `release` mode
      commands: [ezd run rust:arm-unknown-linux-musleabihf -- "cargo zigbuild --target=arm-unknown-linux-musleabihf --release"]
    build/x86_64-apple-darwin/debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:x86_64-apple-darwin -- "cargo zigbuild --target=x86_64-apple-darwin"]
    build/x86_64-apple-darwin/release:
      description: build the application in `release` mode
      commands: [ezd run rust:x86_64-apple-darwin -- "cargo zigbuild --target=x86_64-apple-darwin --profile=release-darwin"]
    build/aarch64-apple-darwin/debug:
      description: build the application in `debug` mode
      commands: [ezd run rust:aarch64-apple-darwin -- "cargo zigbuild --target=aarch64-apple-darwin"]
    build/aarch64-apple-darwin/release:
      description: build the application in `release` mode
      commands: [ezd run rust:aarch64-apple-darwin -- "cargo zigbuild --target=aarch64-apple-darwin --profile=release-darwin"]
  docker_seq:
    rust:x86_64-unknown-linux-musl:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.65.0-x86_64-unknown-linux-musl
        pull: always
        env:
          - SCCACHE_DIR=/sccache
        volumes:
          - .:/app
          - cache_ezd-rs:/usr/local/cargo/registry
          - sccache:/sccache
        workdir: /app
        sequence:
          - user: 0:0
            commands: [adduser -D -u $EZD_HOST_UID $EZD_HOST_USERNAME]
          - local_user: true
            commands: [eval "set -eux && $EZD_FORWARDED_ARGS"]
    rust:aarch64-unknown-linux-musl:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.65.0-aarch64-unknown-linux-musl
        pull: always
        env:
          - SCCACHE_DIR=/sccache
        volumes:
          - .:/app
          - cache_ezd-rs:/usr/local/cargo/registry
          - sccache:/sccache
        workdir: /app
        sequence:
          - user: 0:0
            commands: [adduser -D -u $EZD_HOST_UID $EZD_HOST_USERNAME]
          - local_user: true
            commands: [eval "set -eux && $EZD_FORWARDED_ARGS"]
    rust:arm-unknown-linux-musleabihf:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.65.0-arm-unknown-linux-musleabihf
        pull: always
        env:
          - SCCACHE_DIR=/sccache
        volumes:
          - .:/app
          - cache_ezd-rs:/usr/local/cargo/registry
          - sccache:/sccache
        workdir: /app
        sequence:
          - user: 0:0
            commands: [adduser -D -u $EZD_HOST_UID $EZD_HOST_USERNAME]
          - local_user: true
            commands: [eval "set -eux && $EZD_FORWARDED_ARGS"]
    rust:x86_64-apple-darwin:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.65.0-x86_64-apple-darwin
        pull: always
        env:
          - SCCACHE_DIR=/sccache
        volumes:
          - .:/app
          - cache_ezd-rs:/usr/local/cargo/registry
          - sccache:/sccache
        workdir: /app
        sequence:
          - user: 0:0
            commands: [adduser -D -u $EZD_HOST_UID $EZD_HOST_USERNAME]
          - local_user: true
            commands: [eval "set -eux && $EZD_FORWARDED_ARGS"]
    rust:aarch64-apple-darwin:
      description: execute commands within a builder container
      config:
        image: registry.gitlab.com/sbenv/veroxis/images/sdks/rust:1.65.0-aarch64-apple-darwin
        pull: always
        env:
          - SCCACHE_DIR=/sccache
        volumes:
          - .:/app
          - cache_ezd-rs:/usr/local/cargo/registry
          - sccache:/sccache
        workdir: /app
        sequence:
          - user: 0:0
            commands: [adduser -D -u $EZD_HOST_UID $EZD_HOST_USERNAME]
          - local_user: true
            commands: [eval "set -eux && $EZD_FORWARDED_ARGS"]

volumes:
  cache_ezd-rs:
  sccache:
