use std::fs::read_to_string;

use clap::Parser;
use ezd_core::config::ezd::find_config_path;
use ezd_core::config::ezd::Ezd;
use ezd_core::config::ezd::EzdError;
use ezd_core::config::ezd::FindConfigPathError;
use thiserror::Error;

use crate::utils::print_similar_tasks_help;
#[derive(Debug, Parser)]
#[command(trailing_var_arg = true)]
pub struct RunArgs {
    /// task to execute
    #[arg(name = "task")]
    pub task_name: String,
    /// fowarded args
    #[arg()]
    pub forwarded_args: Vec<String>,
}

#[derive(Debug, Error)]
pub enum RunError {
    #[error("Io: `{0}`")]
    Io(#[from] std::io::Error),

    #[error("FindConfigPath: `{0}`")]
    FindConfigPath(#[from] FindConfigPathError),

    #[error("Ezd: `{0}`")]
    Ezd(#[from] EzdError),
}

pub fn run(args: RunArgs) -> Result<(), RunError> {
    let forwarded_args = args.forwarded_args.join(" ");
    let config_path = find_config_path()?;
    let config = read_to_string(config_path)?;
    let mut ezd = Ezd::try_from(config)?;
    ezd.add_env("EZD_FORWARDED_ARGS".into(), forwarded_args);
    let all_tasks = ezd.get_tasks();
    if all_tasks.contains(&args.task_name) {
        ezd.run(args.task_name.as_str())?;
    } else {
        print_similar_tasks_help(&ezd, args.task_name.as_str());
        return Err(EzdError::TaskDoesNotExistError {
            task: args.task_name,
        }
        .into());
    }
    Ok(())
}
