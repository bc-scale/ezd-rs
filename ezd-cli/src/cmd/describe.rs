use std::fs::read_to_string;

use clap::Parser;
use ezd_core::config::ezd::find_config_path;
use ezd_core::config::ezd::Ezd;
use ezd_core::config::ezd::EzdError;
use ezd_core::config::ezd::FindConfigPathError;
use thiserror::Error;

use crate::utils::print_similar_tasks_help;
use crate::utils::PrettyPrint;

#[derive(Debug, Parser)]
pub struct DescribeArgs {
    /// an optional specific task to describe
    #[arg(name = "task")]
    pub task_name: Option<String>,
}

#[derive(Debug, Error)]
pub enum DescribeError {
    #[error("Io: `{0}`")]
    Io(#[from] std::io::Error),

    #[error("FindConfigPath: `{0}`")]
    FindConfigPath(#[from] FindConfigPathError),

    #[error("SerdeJson: `{0}`")]
    SerdeJson(#[from] serde_json::Error),

    #[error("Ezd: `{0}`")]
    Ezd(#[from] EzdError),

    #[error("TaskDoesNotExist: `{0}`")]
    TaskDoesNotExist(String),
}

pub fn describe(args: DescribeArgs) -> Result<(), DescribeError> {
    let config_path = find_config_path()?;
    let config = read_to_string(config_path)?;
    let ezd = Ezd::try_from(config)?;

    if let Some(task_name) = args.task_name {
        match ezd.get_task_by_name(task_name.as_str()) {
            ezd_core::config::ezd::EzdTask::ShellExec(task) => {
                serde_json::json!(task).pretty_print()?;
            }
            ezd_core::config::ezd::EzdTask::DockerSeq(task) => {
                serde_json::json!(task).pretty_print()?;
            }
            ezd_core::config::ezd::EzdTask::None => {
                print_similar_tasks_help(&ezd, task_name.as_str());
                return Err(DescribeError::TaskDoesNotExist(task_name));
            }
        }
    } else {
        serde_json::json!(ezd).pretty_print()?;
    }
    Ok(())
}
