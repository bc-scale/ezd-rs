use std::collections::BTreeMap;
use std::fs::read_to_string;

use ezd_core::config::ezd::find_config_path;
use ezd_core::config::ezd::Ezd;
use ezd_core::config::ezd::EzdError;
use ezd_core::config::ezd::FindConfigPathError;
use thiserror::Error;

use crate::utils::PrettyPrint;

#[derive(Debug, Error)]
pub enum ListError {
    #[error("Io: `{0}`")]
    Io(#[from] std::io::Error),

    #[error("FindConfigPath: `{0}`")]
    FindConfigPath(#[from] FindConfigPathError),

    #[error("SerdeJson: `{0}`")]
    SerdeJson(#[from] serde_json::Error),

    #[error("Ezd: `{0}`")]
    Ezd(#[from] EzdError),
}

pub fn list() -> Result<(), ListError> {
    let config_path = find_config_path()?;
    let config = read_to_string(config_path)?;
    let ezd = Ezd::try_from(config)?;
    let mut tasks: BTreeMap<&str, &str> = BTreeMap::new();
    for (name, task) in ezd.tasks.shell_exec.iter() {
        tasks.insert(name.as_str(), task.description.as_str());
    }
    for (name, task) in ezd.tasks.docker_seq.iter() {
        tasks.insert(name.as_str(), task.description.as_str());
    }
    serde_json::json!(tasks).pretty_print()?;
    Ok(())
}
