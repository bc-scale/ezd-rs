use std::env;
use std::path::PathBuf;

use clap::Parser;
use cmd::describe::DescribeArgs;
use cmd::describe::DescribeError;
use cmd::init::InitError;
use cmd::list::ListError;
use cmd::run::RunArgs;
use cmd::run::RunError;
use colored::Colorize;
use thiserror::Error;

mod cmd;
mod utils;

#[derive(Debug, Parser)]
#[command(name = "ezd", author, version, about, long_about = None)]
pub struct EzdArguments {
    /// set the working directory for ezd to be executed from
    #[arg(short, long)]
    pub workdir: Option<PathBuf>,

    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Parser, Debug)]
#[command(name = "commands", about)]
pub enum Commands {
    /// execute a task
    #[command(name = "run")]
    Run(RunArgs),
    /// list all tasks
    #[command(name = "list")]
    List,
    /// describe all tasks
    #[command(name = "describe")]
    Describe(DescribeArgs),
    /// create an empty config in the current directory
    #[command(name = "init")]
    Init,
}

#[derive(Debug, Error)]
pub enum CliCommandError {
    #[error("WorkdirError: `{0}`")]
    WorkdirError(String),

    #[error("RunError: `{0}`")]
    RunError(#[from] RunError),

    #[error("ListError: `{0}`")]
    ListError(#[from] ListError),

    #[error("DescribeError: `{0}`")]
    DescribeError(#[from] DescribeError),

    #[error("InitError: `{0}`")]
    InitError(#[from] InitError),
}

fn main() -> Result<(), CliCommandError> {
    let args = EzdArguments::parse();
    if let Some(workdir) = args.workdir {
        env::set_current_dir(workdir).map_err(|e| CliCommandError::WorkdirError(e.to_string()))?;
    }
    let result = match args.command {
        Commands::Run(args) => cmd::run::run(args).map_err(CliCommandError::RunError),
        Commands::List => cmd::list::list().map_err(CliCommandError::ListError),
        Commands::Describe(args) => {
            cmd::describe::describe(args).map_err(CliCommandError::DescribeError)
        }
        Commands::Init => cmd::init::init().map_err(CliCommandError::InitError),
    };
    if let Err(error) = result {
        let formatted_error_message = format!("{:?}", error);
        eprintln!(
            "[{}:{}] {}",
            "ezd".green(),
            "error".bold().red(),
            formatted_error_message.red()
        );
        std::process::exit(1);
    }
    Ok(())
}
